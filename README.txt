Assumptions:

- I did not know whether you wanted a simple-as-possible solution or not. I used Spring Framework to spice it up a bit.
- You have Linux, Maven and Java setup on the computer which is going to run this application


To compile:

- Make the build.sh file executable and run it.



To run the application: 
 
- Make the run.sh file executable and run it, passing in a filename as a parameter.



Note: 

- The application creates a amazon-operations.log file in the current directory when it is executed. The log file contains an audit trail of the execution.