package za.co.aljordaan.operations.annotation;

import org.springframework.beans.factory.annotation.Qualifier;
import za.co.aljordaan.operations.objectmodel.Operator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface OperationQualifier
{
    Operator operator();
}
