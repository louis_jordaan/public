package za.co.aljordaan.operations;

import com.google.common.collect.Lists;
import com.google.common.io.Files;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import za.co.aljordaan.operations.component.LineProcessor;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

public class Application
{
  public static void main(String[] args)
  {
    // Check if a filename was provided
    if (args.length < 1)
    {
      System.out.println("Please provide the filename of the input file.");
      return;
    }

    // Check if the file exists
    File inputFile = new File(args[0]);
    if (!inputFile.exists())
    {
      System.out.println("The file does not exist.");
      return;
    }

    // Load the Spring application context
    ApplicationContext context = new ClassPathXmlApplicationContext("spring/application-context.xml");
    LineProcessor processor = context.getBean(LineProcessor.class);

    // Read the lines in the file
    List<String> lines = Lists.newArrayList();
    try
    {
      lines.addAll(Files.readLines(inputFile, Charset.forName("UTF-8")));
    }
    catch (IOException e)
    {
      System.out.println("An error occurred while reading the file: " + e.getMessage());
    }

    // Process each of the lines
    for (String line : lines)
    {
      try
      {
        System.out.println(processor.process(line));
      }
      catch (Exception ex)
      {
        System.out.println(ex);
      }
    }
  }
}
