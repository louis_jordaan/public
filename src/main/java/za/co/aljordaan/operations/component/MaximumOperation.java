package za.co.aljordaan.operations.component;

import org.springframework.stereotype.Component;
import za.co.aljordaan.operations.annotation.OperationQualifier;
import za.co.aljordaan.operations.objectmodel.Operator;

@Component("maximumOperation")
@OperationQualifier(operator = Operator.MAX)
public class MaximumOperation implements IOperation
{

  @Override
  public double calculate(final double[] arguments)
  {
    return maximum(0, arguments);
  }

  private double maximum(final int index, final double[] arguments)
  {
    if (arguments == null || arguments.length == 0)
    {
      throw new IllegalArgumentException("Cannot calculate the maximum value, no values provided.");
    }
    if (index < 0 || index >= arguments.length)
    {
      throw new IllegalArgumentException("Cannot calculate the maximum value, invalid index value: " + index);
    }
    else
    {
      if (index + 1 < arguments.length)
      {
        return Math.max(arguments[index], maximum(index + 1, arguments));
      }
      else
      {
        return arguments[index];
      }
    }
  }
}
