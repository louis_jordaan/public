package za.co.aljordaan.operations.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.aljordaan.operations.annotation.OperationQualifier;
import za.co.aljordaan.operations.objectmodel.Operator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class LineProcessor
{
  @Autowired
  @OperationQualifier(operator = Operator.AVERAGE)
  private IOperation averageOperation;

  @Autowired
  @OperationQualifier(operator = Operator.MIN)
  private IOperation minimumOperation;

  @Autowired
  @OperationQualifier(operator = Operator.MAX)
  private IOperation maximumOperation;

  @Autowired
  @OperationQualifier(operator = Operator.SUM)
  private IOperation sumOperation;

  public String process(final String line)
  {
    // Only process the line if the format is correct
    if (validate(line))
    {

      Operator operator = determineOperator(line);
      double[] arguments = determineArguments(line);

      double result;
      switch (operator)
      {
        case AVERAGE:
          result = averageOperation.calculate(arguments);
          break;
        case SUM:
          result = sumOperation.calculate(arguments);
          break;
        case MIN:
          result = minimumOperation.calculate(arguments);
          break;
        case MAX:
          result = maximumOperation.calculate(arguments);
          break;
        default:
          throw new RuntimeException("Operation not supported: " + (operator == null ? "null" : operator.getId()));
      }

      return operator.getId() + " " + result;

    }
    else
    {
      throw new IllegalArgumentException("Invalid input format. Format should be: '[SUM|MIN|MAX|AVERAGE] : [comma-separated list of numbers]'");
    }
  }

  private boolean validate(final String line)
  {
    StringBuilder regex = new StringBuilder();
    regex.append("^((")
        .append(Operator.SUM.getId())
        .append("|")
        .append(Operator.MIN.getId())
        .append("|")
        .append(Operator.MAX.getId())
        .append("|")
        .append(Operator.AVERAGE.getId())
        .append(")\\s*:\\s*\\d+(\\.\\d+)?(\\s*,\\s*\\d+(\\.\\d+)?)*)$");

    Pattern pattern = Pattern.compile(regex.toString());
    Matcher matcher = pattern.matcher(line);
    return matcher.find();
  }

  private double[] determineArguments(final String line)
  {
    int colonIndex = line.indexOf(":");

    if (colonIndex >= 0)
    {
      if (colonIndex + 1 < line.length())
      {
        String[] tokens = line.substring(colonIndex + 1).split(",");
        if (tokens.length > 0)
        {
          double[] arguments = new double[tokens.length];
          for (int i = 0; i < tokens.length; i++)
          {
            try
            {
              arguments[i] = Double.parseDouble(tokens[i].trim());
            }
            catch (NumberFormatException ex)
            {
              throw new RuntimeException("Invalid argument provided: '" + tokens[i] + "'");
            }
          }

          return arguments;
        }
      }
    }
    else
    {
      throw new RuntimeException("Missing ':' after operator.");
    }

    throw new RuntimeException("No arguments provided.");
  }

  private Operator determineOperator(final String line)
  {
    int colonIndex = line.indexOf(":");

    if (colonIndex > 0)
    {
      String operatorId = line.substring(0, colonIndex).trim();
      Operator operator = Operator.findById(operatorId);

      if (operator != null)
      {
        return operator;
      }
      else
      {
        throw new RuntimeException("Operator not supported: " + operatorId);
      }
    }

    throw new RuntimeException("No operator specified.");
  }

}
