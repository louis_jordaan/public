package za.co.aljordaan.operations.component;

import org.springframework.stereotype.Component;
import za.co.aljordaan.operations.annotation.OperationQualifier;
import za.co.aljordaan.operations.objectmodel.Operator;

@Component("averageOperation")
@OperationQualifier(operator = Operator.AVERAGE)
public class AverageOperation implements IOperation
{
  @Override
  public double calculate(final double[] arguments)
  {
    if (arguments == null || arguments.length == 0)
    {
      throw new IllegalArgumentException("Cannot calculate the average value, no values provided.");
    }

    return (new SummationOperation().calculate(arguments)) / arguments.length;
  }
}
