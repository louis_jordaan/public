package za.co.aljordaan.operations.component;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author adriaan5
 */
@Aspect
@Component
public class AuditInterceptor
{

  private static final Logger LOGGER = LoggerFactory.getLogger(AuditInterceptor.class);


  public AuditInterceptor()
  {
    LOGGER.debug("**** Starting audit trail ****");
  }

  // Point cuts:

  @Pointcut("within(za.co.aljordaan.operations..*)")
  public void withinOperationsPackage()
  {
    // A pointcut for any method in a class in the za.co.aljordaan.operations package, or sub-packages thereof.
  }


  // Advices:
  /**
   * @param joinPoint
   * @param throwable Can be used to restrict matching using the exception type.
   *                  Currently using Throwable to match on ANY exception.
   */
  @AfterThrowing(
      pointcut = "withinOperationsPackage()",
      throwing = "throwable")
  public void afterThrowingFromOperationsClassMethod(final JoinPoint joinPoint, final Throwable throwable)
  {
    LOGGER.error(String.format("Exception occurred in [%s].%nException [%s].%nArguments = [%s]", joinPoint.getSignature(), throwable, Arrays.toString(joinPoint.getArgs())));
  }

  @Before("withinOperationsPackage()")
  public void enteringExecution(final JoinPoint joinPoint){
    LOGGER.error(String.format("Starting execution of [%s].%nArguments = [%s]", joinPoint.getSignature(), Arrays.toString(joinPoint.getArgs())));
  }

  @AfterReturning(pointcut = "withinOperationsPackage()",
                  returning = "result")
  public void returningExecution(final JoinPoint joinPoint, Object result){
    LOGGER.error(String.format("Exiting execution of [%s].%nResult = [%s]", joinPoint.getSignature(), result));
  }



}
