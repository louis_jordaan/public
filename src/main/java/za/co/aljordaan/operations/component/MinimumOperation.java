package za.co.aljordaan.operations.component;

import org.springframework.stereotype.Component;
import za.co.aljordaan.operations.annotation.OperationQualifier;
import za.co.aljordaan.operations.objectmodel.Operator;

@Component("minimumOperation")
@OperationQualifier(operator = Operator.MIN)
public class MinimumOperation implements IOperation
{

  @Override
  public double calculate(final double[] arguments)
  {
    return minimum(0, arguments);
  }

  private double minimum(final int index, final double[] arguments)
  {
    if (arguments == null || arguments.length == 0)
    {
      throw new IllegalArgumentException("Cannot calculate the minimum value, no values provided.");
    }
    else if (index < 0 || index >= arguments.length)
    {
      throw new IllegalArgumentException("Cannot calculate the minimum value, invalid index value: " + index);
    }
    else
    {
      if (index + 1 < arguments.length)
      {
        return Math.min(arguments[index], minimum(index + 1, arguments));
      }
      else
      {
        return arguments[index];
      }
    }
  }

}
