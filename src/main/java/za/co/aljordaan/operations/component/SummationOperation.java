package za.co.aljordaan.operations.component;

import org.springframework.stereotype.Component;
import za.co.aljordaan.operations.annotation.OperationQualifier;
import za.co.aljordaan.operations.objectmodel.Operator;

@Component("summationOperation")
@OperationQualifier(operator = Operator.SUM)
public class SummationOperation implements IOperation
{

  @Override
  public double calculate(final double[] arguments)
  {
    double sum = 0;

    if (arguments != null)
    {
      for (double value : arguments)
      {
        sum += value;
      }
    }

    return sum;
  }
}
