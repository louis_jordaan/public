package za.co.aljordaan.operations.component;

public interface IOperation
{
  double calculate(final double[] arguments);
}
