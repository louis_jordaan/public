package za.co.aljordaan.operations.objectmodel;

import com.google.common.base.Objects;

public enum Operator
{
  SUM("SUM", "The sum of the values"),
  MIN("MIN", "The minimum value"),
  MAX("MAX", "The maximum value"),
  AVERAGE("AVERAGE", "The average of the values");

  private String id;
  private String description;

  private Operator(String id, String description)
  {
    this.id = id;
    this.description = description;
  }

  public String getId()
  {
    return id;
  }

  public String getDescription()
  {
    return description;
  }

  /**
   * Finds the Operator with the specified id. The search is case sensitive.
   * @param id
   * @return Operator whose id matches the id parameter.
   */
  public static Operator findById(final String id)
  {
    for (Operator op : values())
    {
      if (Objects.equal(op.getId(), id))
      {
        return op;
      }
    }

    // No operator with the specified id exists
    return null;
  }
}
