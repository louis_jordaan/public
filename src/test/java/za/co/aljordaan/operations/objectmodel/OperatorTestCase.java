package za.co.aljordaan.operations.objectmodel;

import org.junit.Assert;
import org.junit.Test;

public class OperatorTestCase
{
  @Test
  public void testFindByCode_validId()
  {
    System.out.println("*** testFindByCode_validId() ***");

    String id = "AVERAGE";
    System.out.println("Getting Operator with id: " + id);
    Operator result = Operator.findById(id);
    System.out.println("Result: " + result);
    Assert.assertEquals("The incorrect operator was returned.", Operator.AVERAGE, result);
  }

  @Test
  public void testFindByCode_incorrectId()
  {
    System.out.println("*** testFindByCode_incorrectId() ***");

    String id = "AVG";
    System.out.println("Getting Operator with id: " + id);
    Operator result = Operator.findById(id);
    System.out.println("Result: " + result);
    Assert.assertEquals("The incorrect operator was returned.", null, result);
  }

  @Test
  public void testFindByCode_nullId()
  {
    System.out.println("*** testFindByCode_nullId() ***");

    System.out.println("Getting Operator with id: " + null);
    Operator result = Operator.findById(null);
    System.out.println("Result: " + result);
    Assert.assertEquals("The incorrect operator was returned.", null, result);
  }

  @Test
  public void testFindByCode_emptyId()
  {
    System.out.println("*** testFindByCode_emptyId() ***");

    String id = "";
    System.out.println("Getting Operator with id: " + id);
    Operator result = Operator.findById(id);
    System.out.println("Result: " + result);
    Assert.assertEquals("The incorrect operator was returned.", null, result);
  }
}
