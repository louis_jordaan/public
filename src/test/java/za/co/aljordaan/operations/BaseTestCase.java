package za.co.aljordaan.operations;

import org.junit.Before;

import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class BaseTestCase<T>
{
  protected T instance;

  @Before
  public void setUp() throws Exception
  {
    instance = (T) createGenericTypeInstance(getClass(), 0);
  }

  public Object createGenericTypeInstance(Class<?> clazz, int genericTypeIndex)
  {
    Type type = clazz.getGenericSuperclass();

    if (type instanceof ParameterizedType)
    {
      ParameterizedType parameterizedType = (ParameterizedType) type;
      Class<?> resultClass = (Class<?>) parameterizedType.getActualTypeArguments()[genericTypeIndex];

      return createInstance(resultClass);
    }

    throw new RuntimeException("Could not create generic instance , " + clazz + " at the spesified index, " +
        genericTypeIndex);
  }

  public Object createInstance(Class instanceClass)
  {
    try
    {
      Class<?> classToInstantiate = Class.forName(instanceClass.getName());
      Constructor<?>[] declaredConstructors = classToInstantiate.getDeclaredConstructors();

      // We are searching through all of the declared constructors and we are trying to find the no-argument constructor
      for (Constructor<?> declaredConstructor : declaredConstructors)
      {
        if (declaredConstructor.getParameterTypes().length == 0)
        {
          declaredConstructor.setAccessible(true);
          return declaredConstructor.newInstance();
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new RuntimeException("Could not instantiate class", e);
    }

    throw new RuntimeException("Could not Instantiate class");
  }
}
