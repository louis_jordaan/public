package za.co.aljordaan.operations.component;


import org.junit.Assert;
import org.junit.Test;
import za.co.aljordaan.operations.BaseTestCase;

import java.util.Arrays;

public class SummationOperationImplTestCase extends BaseTestCase<SummationOperation>
{


  @Test
  public void testCalculate_multipleInputs()
  {
    System.out.println("*** testCalculate_multipleInputs() ***");

    double[] input = new double[]{15, 3, 41, 8, 33};
    System.out.println("input: " + Arrays.toString(input));
    Double output = instance.calculate(input);
    System.out.println("output: " + output);

    Assert.assertEquals("The sum of the values is wrong", 100, output, 0);
  }

  @Test
  public void testCalculate_singleInput()
  {
    System.out.println("*** testCalculate_singleInput() ***");

    double[] input = new double[]{15};
    System.out.println("input: " + Arrays.toString(input));
    Double output = instance.calculate(input);
    System.out.println("output: " + output);

    Assert.assertEquals("The sum of the values is wrong", 15, output, 0);
  }

  @Test
  public void testCalculate_noInputs()
  {
    System.out.println("*** testCalculate_noInputs() ***");

    double[] input = new double[]{};
    System.out.println("input: " + Arrays.toString(input));
    Double output = instance.calculate(input);
    System.out.println("output: " + output);

    Assert.assertEquals("The sum of the values is wrong", 0, output, 0);
  }

  @Test
  public void testCalculate_nullInputs()
  {
    System.out.println("*** testCalculate_nullInput() ***");

    System.out.println("input: " + null);
    Double output = instance.calculate(null);
    System.out.println("output: " + output);

    Assert.assertEquals("The sum of the values is wrong", 0, output, 0);
  }
}
