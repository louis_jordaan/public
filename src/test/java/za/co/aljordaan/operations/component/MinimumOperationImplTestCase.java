package za.co.aljordaan.operations.component;


import org.junit.Assert;
import org.junit.Test;
import za.co.aljordaan.operations.BaseTestCase;

import java.util.Arrays;

public class MinimumOperationImplTestCase extends BaseTestCase<MinimumOperation>
{


  @Test
  public void testCalculate_multipleInputs()
  {
    System.out.println("*** testCalculate_multipleInputs() ***");

    double[] input = new double[]{-5, 15, 3, -5, 41, 8, 33};

    System.out.println("input: " + Arrays.toString(input));
    Double output = instance.calculate(input);
    System.out.println("output: " + output);

    Assert.assertEquals("The minimum value is wrong", new Double(-5), output);
  }

  @Test
  public void testCalculate_singleInput()
  {
    System.out.println("*** testCalculate_singleInput() ***");

    double[] input = new double[]{15};

    System.out.println("input: " + Arrays.toString(input));
    Double output = instance.calculate(input);
    System.out.println("output: " + output);

    Assert.assertEquals("The minimum value is wrong", new Double(15.0), output);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testCalculate_noInputs()
  {
    System.out.println("*** testCalculate_noInputs() ***");

    double[] input = new double[]{};

    System.out.println("input: " + Arrays.toString(input));
    instance.calculate(input);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testCalculate_nullInputs()
  {
    System.out.println("*** testCalculate_nullInput() ***");

    System.out.println("input: " + null);
    instance.calculate(null);
  }
}
